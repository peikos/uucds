#!/usr/bin/env python2.7
# coding: utf-8
from __future__ import print_function

from collections import *

import codecs
import collections
import math
import os
import time
import itertools

import numpy as np
import cPickle as pickle

from conllu import parse

from scipy.sparse.lil import lil_matrix
from scipy.sparse import vstack

from composes.utils import io_utils
from composes.semantic_space.space import Space
from composes.composition.lexical_function import LexicalFunction
from composes.transformation.dim_reduction.nmf import Nmf

from HTMLParser import HTMLParser

class SpaceBuilder(object):
    @staticmethod
    def interesting(w):
        """Stopwords can be provided on parameterisation, punctuation and
           articles are always ignored based on UD tag."""
        return w[u"upostag"] != u"DET" and w[u"upostag"] != u"PUNCT"

    @staticmethod
    def label(w):
        return w[u"lemma"] + "_" + w[u"upostag"]

    def create_dictionary(self, min_occurence = None):
        """Create a dictionary based on all lemmas occuring a minimum number
           of times."""
        if not min_occurence: # Override min_occurence, e.g. for rows
            min_occurence = self.min_occurence
        if not min_occurence or not self.corpus:
            raise ValueError("corpus and min_occurence need to be set to \
                    create a dictionary!")
        dictionary = []
        words = [item for sublist in self.corpus for item in sublist]
        report("Determining rows and optimal columns")
        for w in words:
            if SpaceBuilder.interesting(w):
                #print("Adding", w) #NEW
                dictionary.append(SpaceBuilder.label(w))
        d = set([item for item, count in collections.Counter(dictionary).items() if count >= min_occurence])
        done()
        return list(d - self.stopwords)

    def count_words(self):
        """Count words based on the couting model used."""
        if not self.columns or not self.rows:
            if self.min_occurence and self.corpus:
                print("Creating columns") # NEW
                self.columns = self.create_dictionary()
                # Only words occuring often enough to consider
                print("Creating row") # NEW
                self.rows    = self.create_dictionary(1)
                # All interesting words
            else:
                raise ValueError("columns and rows are required to be set \
                        to count words. Make sure create_dictionary() has \
                        been run before trying to run count_words()!")

        report("Counting cooccurrences")
        self.counts = lil_matrix((len(self.rows),len(self.columns)),
                dtype=np.float)
        #nr = 0 # NEW
        for sen in self.corpus:
            #print("Counting sentence", nr) # NEW
            #nr += 1 # NEW
            for word in sen:
                ops = self.count_when.run(sen, word, self.rows, self.columns)
                for (r, c, v) in ops:     # Instead of passing the matrix
                    self.counts[r,c] += v # around, only pass the changes.
        done()

    def features(self):
        """Alias for columns."""
        return len(self.columns)

    def make_dissect_space(self):
        """Convert counts to DISSECT space."""

        if not self.columns or not self.rows or self.counts is None:
            raise ValueError("Make sure to run count_words() before \
                    attempting to convert to a DISSECT-space!")

        report("Making sure data directory exists and is empty")
        path = os.path.dirname("data")
        if os.path.isdir(path) and not os.listdir(path) == []:
            os.rename(path, path + "_backup." + int(time.time()))
        if os.path.isfile(path):
            os.rename(path, path + "_backup." + int(time.time()))
        try:
            os.makedirs("data")
        except OSError:
            pass
        done()

        report("Generating DISSECT space")
        with open("data/temp.rows", "w") as output:
            output.write("\n".join(self.rows).encode("utf8"))

        with open("data/temp.cols", "w") as output:
            output.write("\n".join(self.columns).encode("utf8"))

        i = self.counts.nonzero()
        i = zip(i[0], i[1])

        sm = map(lambda (x, y, w): "{} {} {:d}".format(x.encode("utf8")
                                                      ,y.encode("utf8")
                                                      ,int(w))
                , map(lambda (x,y): (self.rows[x]
                                    , self.columns[y]
                                    , self.counts[x,y]), i))

        with open("data/temp.sm", "w") as output:
            output.write("\n".join(sm))

        self.import_dissect_space("data/temp.sm",
                                  "data/temp.rows",
                                  "data/temp.cols")
        done()

    def import_dissect_space(self, sm, r, c):
        """Helper function to import a DISSECT space. This should probably
           be streamlined in a later version."""
        self.dissect = Space.build(data = sm,
                                      rows = r,
                                      cols = c,
                                      format = "sm")

    def build_phrase_space(self):
        if not self.columns:
            if self.min_occurence and self.corpus:
                self.columns = self.create_dictionary()
            else:
                raise ValueError("columns is required to be set. Make sure \
                        create_dictionary() has been run before trying to \
                        build a phrase space!")

        def add_to_matrix(v, o):
            """Internal function to destructively update the count matrix"""

            print("Found", v, "and", o)
            if v in self.tv_rows and o in self.columns: # Update count
                r = self.tv_rows.index(v)
                c = self.columns.index(o)
                self.tv_counts[(r,c)] += 1
            elif o in self.columns:                      # Add new row
                new_row = lil_matrix((1,len(self.columns)), dtype=np.float)
                new_row[0, self.columns.index(o)] = 1
                self.tv_rows.append(v)
                self.tv_counts = vstack([self.tv_counts, new_row], "lil")

        def verb(w, s):
            """Get the verb this nsubj/obj is associated with.
               In some cases, deprel will point towards a conjunction,
               but dep will include a link to the verb."""
            for (d, r) in w[u"deps"]:
                if (d == u"nsubj" or d == u"obj") \
                        and s[r-1]["upostag"] == "VERB":
                    return s[r-1]   # Immediately return when verb found.
            return None             # Nothing found.

        def noneIsEmpty(xs):
            """Convenient helper-function."""
            if xs == None:
                return []
            else:
                return xs

        report("Counting V/O cooccurrences")
        self.tv_rows = []
        self.tv_counts = lil_matrix((0, len(self.columns)), dtype=np.float)
        for sen in self.corpus:
            if len(sen) > 0:
                # ss = [s for s in sen if u"nsubj" in [d[0]
                    # for d in noneIsEmpty(s[u"deps"])]]
                os = [o for o in sen if u"obj" in [d[0]
                    for d in noneIsEmpty(o[u"deps"])]]
                tups = [(verb(s, sen),o) for s in os if verb(s, sen)]
                for (v, o) in tups:
                    if SpaceBuilder.interesting(o):
                        add_to_matrix("_" + SpaceBuilder.label(v),
                                            SpaceBuilder.label(o))
        done()

    def make_dissect_phrase_space(self):
        """Convert verb counts to DISSECT space."""

        if not self.columns or not self.tv_rows or self.tv_counts is None:
            raise ValueError("Make sure to run build_phrase_space() before \
                    attempting to convert to a DISSECT-space!")

        report("Making sure data directory exists and is empty")
        path = os.path.dirname("data")
        if os.path.isdir(path) and not os.listdir(path) == []:
            os.rename(path, path + "_backup." + int(time.time()))
        if os.path.isfile(path):
            os.rename(path, path + "_backup." + int(time.time()))
        try:
            os.makedirs("data")
        except OSError:
            pass
        done()

        report("Generating DISSECT space for verbs")
        with open("data/tvtemp.rows", "w") as output:
            output.write("\n".join(self.tv_rows).encode("utf8"))

        with open("data/tvtemp.cols", "w") as output:
            output.write("\n".join(self.columns).encode("utf8"))

        i = self.tv_counts.nonzero()
        i = zip(i[0], i[1])

        sm = map(lambda (x, y, w): "{} {} {:d}".format(x.encode("utf8")
                                                      ,y.encode("utf8")
                                                      ,int(w))
                , map(lambda (x,y): ( self.tv_rows[x]
                                    , self.columns[y]
                                    , self.tv_counts[x,y]), i))

        with open("data/tvtemp.sm", "w") as output:
            output.write("\n".join(sm))

        self.import_dissect_phrase_space("data/tvtemp.sm",
                                         "data/tvtemp.rows",
                                         "data/tvtemp.cols")
        done()

    def import_dissect_phrase_space(self, sm, r, c):
        """Helper function to import a DISSECT phrase-space. This should
           probably be streamlined in a later version."""
        self.tv_dissect = Space.build(data = sm,
                                      rows = r,
                                      cols = c,
                                      format = "sm")

    def reduce_dissect(self, k = None):
        """Reduce the dissect space dimensionality to k columns."""
        if k == None:
            k = self.target_space

        if not self.dissect or not self.tv_dissect:
            raise ValueError("dissect and tv_dissect are required to be set \
                    in order to reduce the feature space!")
        report("Applying dimension-reduction")
        temp = Space.vstack(self.dissect, self.tv_dissect)
        temp2 = temp.apply(Nmf(k))
        temp2.id2_column = [str(x) for x in range(k)]
        self.reduced = Space(temp2.get_rows(self.dissect.get_id2row()),
                self.dissect.get_id2row(), temp2.get_id2column(),
                None, None, temp2.get_operations())
        self.tv_reduced = Space(temp2.get_rows(self.tv_dissect.get_id2row()),
                self.tv_dissect.get_id2row(), temp2.get_id2column(),
                None, None, temp2.get_operations())
        done()

    def make_composed_space(self, directory = None, b = 1000):
        """Train the actual composed space, using b verb-phrase per
           batch and saving all results to directory."""
        if not self.reduced or not self.tv_reduced:
            raise ValueError("reduced and tv_reduced are required to be set \
                    in order to train a composed space! Run reduce_dissect() \
                    first!")

        if directory is None:
            directory = str(time.time())

        report("Making sure directory exists and is empty")
        path = os.path.dirname(directory)
        if os.path.isdir(path) and not os.listdir(path) == []:
            os.rename(path, path + "_backup." + int(time.time()))
        if os.path.isfile(path):
            os.rename(path, path + "_backup." + int(time.time()))
        try:
            os.makedirs(directory)
        except OSError:
            pass
        done()

        report("Generating training data")
        self.train_data = ([(self.tv_rows[v] + "_function",
                             self.rows[o],
                             self.tv_rows[v])
                            for o in range(len(self.rows))]
                            for v in range(len(self.tv_rows)))
        done()

        counter = 0
        self.comp = LexicalFunction()

        report("Training composed space")
        empty = False
        while not empty:
            batch = list(itertools.chain.from_iterable(itertools.islice(
                self.train_data, b)))
            if len(batch) == 0:
                empty = True
            else:
                self.comp.train(batch, self.reduced, self.tv_reduced)
                counter = counter + 1
                print("Saving batch", counter)
                with open(directory + "/" + "output-" + str(counter) + ".pkl",
                        "wb") as output:
                    pickle.dump(self.comp, output, pickle.HIGHEST_PROTOCOL)


    def parameterise(self, corpus, func_obj, min_occurence = 2, t_size = 100,
            stopwords = set()):
        """Provide all the parameters required to run the entire chain, and
           load the CONLL-file containing the corpus"""
        report("Loading Corpus from CONLLU")
        with codecs.open(corpus, "r", encoding="utf-8") as f:
            data = f.read()

        hp = HTMLParser() # Transcode to ASCII, parse and restore Unicode chars.
        parsed = parse(data.encode("ascii", "xmlcharrefreplace"))
        self.corpus = map(lambda sen:
                    map(lambda word:
                      OrderedDict([(k, hp.unescape(v) if isinstance(v, unicode)
                          else v) for (k, v) in word.iteritems()])
                    ,sen)
                  , parsed)

        done()
        self.count_when    = func_obj
        self.min_occurence = min_occurence
        self.stopwords     = stopwords
        self.target_space  = t_size
        self.parameterised = True

    def run(self, directory, k = None):
        """If all parameters are in place, create all required spaces."""
        if k == None:
            k = self.target_space

        if self.parameterised:
            self.columns     = self.create_dictionary()
            # Only words occuring often enough to consider
            self.rows        = self.create_dictionary(1)
            # All interesting words
            self.count_words()
            self.make_dissect_space()
            self.build_phrase_space()
            self.make_dissect_phrase_space()
            self.reduce_dissect(k)
            self.make_composed_space(directory)
        else:
            raise ValueError("Object not parameterised, nothing to run!")

    def __init__(self, inheritFrom = None):
        """Initialise a new instance of Model, optionally using the paramaters
           and results from an existing instance."""
        if inheritFrom:
            report("Inheriting")
            self.parameterised = inheritFrom.parameterised
            self.corpus        = inheritFrom.corpus
            self.count_when    = inheritFrom.count_when
            self.min_occurence = inheritFrom.min_occurence
            self.target_space  = inheritFrom.target_space
            self.stopwords     = inheritFrom.stopwords
            self.columns       = inheritFrom.columns
            self.rows          = inheritFrom.rows
            self.tv_rows       = inheritFrom.tv_rows
            self.counts        = inheritFrom.counts
            self.tv_counts     = inheritFrom.tv_counts
            self.dissect       = inheritFrom.dissect
            self.tv_dissect    = inheritFrom.tv_dissect
            self.reduced       = inheritFrom.reduced
            self.tv_reduced    = inheritFrom.tv_reduced
            done()
        else:
            report("Creating fresh SpaceBuilder")
            self.parameterised = False
            self.corpus        = None
            self.count_when    = None
            self.min_occurence = None
            self.target_space  = None
            self.stopwords     = None
            self.columns       = None
            self.rows          = None
            self.tv_rows       = None
            self.counts        = None
            self.tv_counts     = None
            self.dissect       = None
            self.tv_dissect    = None
            self.reduced       = None
            self.tv_reduced    = None
            done()

class Direct(object):
    """If word is dependent on word in dictionary, count it."""
    def run(self, sen, word, rows, columns):
        w = SpaceBuilder.label(word)
        if word[u"head"]:
            d = SpaceBuilder.label(sen[word[u"head"]-1])
            if SpaceBuilder.interesting(word) and d in columns and w in rows:
                r = rows.index(w)
                c = columns.index(d)
                return [(r, c, 1)]
        return []

class ReverseDirect(object):
    """If word has a dependent word in dictionary, count it."""
    def run(self, sen, word, rows, columns):
        d = SpaceBuilder.label(word)
        if word[u"head"]:
            w = SpaceBuilder.label(sen[word[u"head"]-1])
            if SpaceBuilder.interesting(word) and d in columns and w in rows:
                r = rows.index(w)
                c = columns.index(d)
                return [(r, c, 1)]
        return []

class ExponentialDistance(object):
    """If word is (indirectly) dependent on word in dictionary, count it."""
    def __init__(self, weight):
        self.weight = weight

    def run(self, sen, word, rows, columns):
        w = SpaceBuilder.label(word)
        ops = []
        if SpaceBuilder.interesting(word) and w in rows:
            r = rows.index(w)
            if word[u"head"]:
                dep1 = sen[word[u"head"]-1] # TODO: The should also be a check
                d1 = SpaceBuilder.label(dep1)
                if d1 in columns:
                    c = columns.index(d1)
                    ops.append((r, c, self.weight ** 0))
            else:
                return ops

            if dep1[u"head"]:
                dep2 = sen[dep1[u"head"]-1] # here to see if the head actually
                d2 = SpaceBuilder.label(dep2)
                if d2 in columns:
                    c = columns.index(d2)
                    ops.append((r, c, self.weight ** 1))
            else:
                return ops

            if dep2[u"head"]:
                dep3 = sen[dep2[u"head"]-1] # exists. Introduce Maybe to avoid
                d3 = SpaceBuilder.label(dep3)
                if d3 in columns:
                    c = columns.index(d3)
                    ops.append((r, c, self.weight ** 2))
            else:
                return ops

            if dep3[u"head"]:
                dep4 = sen[dep3[u"head"]-1] # pyramid of nested ifs?
                d4 = SpaceBuilder.label(dep4)
                if d4 in columns:
                    c = columns.index(d4)
                    ops.append((r, c, self.weight ** 3))
        return ops

# http://universaldependencies.org/u/dep/index.html
class DependencyRelation(object):
    """If word is dependent on word in dictionary by a given deprel, count it."""
    def __init__(self, rtype):
        self.rtype = rtype

    def run(self, sen, word, rows, columns):
        w = SpaceBuilder.label(word)
        if word[u"head"]:
            d = SpaceBuilder.label(sen[word[u"head"]-1])
            if (SpaceBuilder.interesting(word) and
                    d in columns and
                    w in rows and
                    word[u"deprel"] == self.rtype):
                r = rows.index(w)
                c = columns.index(d)
                return [(r, c, 1)]
        return []

def report(string):
    print(string, "...", end = " ")

def done():
    print("done")
