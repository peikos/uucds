FROM google/python
EXPOSE 8888

RUN apt-get update -y
RUN apt-get install -y ca-certificates curl

RUN curl https://repo.anaconda.com/archive/Anaconda2-5.2.0-Linux-x86_64.sh -o anaconda.sh
RUN chmod +x anaconda.sh
RUN ./anaconda.sh -b
RUN /root/anaconda2/bin/conda install jupyter -y --quiet

COPY jupyter.docker.config /jupyter_notebook_config.py

RUN /root/anaconda2/bin/pip install --upgrade pip
RUN /root/anaconda2/bin/pip install conllu
RUN /root/anaconda2/bin/pip install git+https://github.com/composes-toolkit/dissect

COPY . uucds
RUN sed -i '5iimport sys\nsys.path.append("/usr/local/lib/python2.7/dist-packages/conllu")\n' /uucds/uucds.py

CMD cd uucds; /root/anaconda2/bin/jupyter notebook --port=8888 --no-browser --allow-root --ip=0.0.0.0 -y --config /jupyter_notebook_config.py .
