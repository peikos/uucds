{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# UUCDS Example Usage and Documentation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import uucds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This module defines the `SpaceBuilder` class, which can be used to parse CONLL-corpora and build DISSECT-spaces based on their contents. The main pillar is the class itself, which is instantiated as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sb = uucds.SpaceBuilder()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As an optional argument, a `SpaceBuilder` can be given an existing `SpaceBuilder` object. This is useful when modifying the class itself, migrating data between different versions of the class. This is intended mainly for migration purposes, as no deep copies are created, but can also be used to copy a finsihed `SpaceBuilder` and diverge at some point (e.g. re-using all counts, but reducing to a differnt number of dimensions)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sb2 = uucds.SpaceBuilder(sb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `SpaceBuilder` class requires a set of arguments to be provided. Some arguments can also be provided when running individual functions, for increased flexibility. The `parameterise()` function takes the following arguments:\n",
    "\n",
    "- A String containing the path to a CONLL-file which will be used as corpus (**n/a**).\n",
    "- An instance of one of the provided classes governing the counting behaviour (`count_when`, see below).\n",
    "- The number of times a word needs to occur to be considered as an initial column (`min_occurence`, defaults to 2).\n",
    "- The number of desired dimensions for the final spaces (`target_size`, defaults to 100).\n",
    "\n",
    "The arguments can also be set manually by setting the values of the instance parameters shown between parentheses. The exception to this rule is the corpus, which is read using the `parameterise()` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sb.parameterise(\"lassy-small.conllu\", uucds.Direct())\n",
    "#sb.count_when = uucds.Direct()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `run()` function will attempt to run the entire toolchain in the correct order. It expects the output directory as an argument, and optionally takes a second argument to define the desired dimensionality of the resulting vectors (defaults to 100).\n",
    "\n",
    "The following functions are called in order:\n",
    "\n",
    "- `create_dictionary()` (twice, once to populate columns using a subset of the encountered words — generally those occuring more than $n$ times; once to populate columns — using anything that is not a stopword)\n",
    "- `count_words()` (to fill the cooccurrence matrix using the couting function provided using `parameterise()`)\n",
    "- `make_dissect_space()` (to convert the counts to DISSECTs format)\n",
    "- `build_phrase_space()` (to fill the cooccurence matrix for verb phrases)\n",
    "- `make_dissect_phrase_space()` (to convert the counts to DISSECTs format)\n",
    "- `reduce_dissect(k)` (to apply dimensionality reduction before regression)\n",
    "- `make_composed_space(directory)` (to apply the regression algorithm on the reduced space and save the results)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sb.run(\"Example\", 100)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also possible to run these functions manually, for example to continue an in-progress space construction from intermediate results, or duplicating a finished `SpaceBuilder` and diverging at some point. The `SpaceBuilder` will generally throw a `ValueError` when a precondition is unmet. In cases where the precondition entails a rather short computation, this computation is executed instead, after which the originally called function continues operation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sb2.parameterise(\"lassy-small.conllu\", uucds.ExponentialDistance(0.5))\n",
    "sb2.columns = sb2.create_dictionary()\n",
    "sb2.rows = sb2.create_dictionary(1)\n",
    "sb2.count_words()\n",
    "sb2.make_dissect_space()\n",
    "sb2.build_phrase_space()\n",
    "sb2.make_dissect_phrase_space()\n",
    "sb2.reduce_dissect() # Optionally provide k here\n",
    "sb2.make_composed_space() # Optionally provide directory here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Internals\n",
    "Internally, the `SpaceBuilder` object keeps most intermediate results: most parameters are time-intensive to compute, but relatively mild on space requirements. The following attributes are available:\n",
    "\n",
    "- `parameterised`, a boolean signifying whether the instance is \"ready to run\"\n",
    "- `corpus`, a UD representation of the corpus\n",
    "- `count_when`, an instance of a Counting Function described below\n",
    "- `min_occurence`, the minimal number of times a word needs to occur to be used as a column (prior to dimensionality reduction)\n",
    "- `target_space`, the desired dimensionality of the output vectors\n",
    "- `stopwords`, a collection of words to ignore\n",
    "- `columns`, a list of labels for the columns (dictionary words)\n",
    "- `rows`, a list of labels for the rows (training examples / target vectors)\n",
    "- `vp_rows`, a list of labels for the rows of the phrase-space\n",
    "- `counts`, a sparse matrix containing the cooccurence counts\n",
    "- `vp_counts`, a sparse matrix containing the cooccurrence counts for the phrase space\n",
    "- `dissect`, a DISSECT space for the argument space\n",
    "- `vp_dissect`, a DISSECT space for the phrase space\n",
    "- `reduced`, the DISSECT argument space after dimensionality reduction\n",
    "- `vp_reduced`, the DISSECT phrase space after dimensionality reduction\n",
    "\n",
    "Functionality such as retrieval of specific vectors and cosine-similarity can be accessed via the DISSECT space objects. The composed space is not kept in memory (as it can be prohibitively large) but is rather written to disk at regular intervals dictated by the batch size. These batches could be reassembled to a complete composed space, or split further into individual verb phrases."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Counting Functions\n",
    "This library provides a set of separate classes, one for each counting function. The provided classes are:\n",
    "\n",
    "- `Direct()`, which counts two words as cooccurring when the row is directly dependent on the dictionary words (column).\n",
    "- `ReverseDirect()`, which counts two words as cooccurring when the dictionary word (column) is directly dependent on the the row.\n",
    "- `ExponentialDistance(w)`, which takes a weight $w$ and counts indirect dependencies: Direct dependencies are counted as $1$, once-removed as $w$, twice-removed as $w^2$ up to thrice-removed as $w^3$. The direction of dependency is as with `Direct()`. The value of $w$ should be in the range $]0,1[$ for this to make sense.\n",
    "- `DependencyRelation(r)`, which takes a UD dependency relation type and only counts cooccurences having this relation. The direction is as with `Direct()`.\n",
    "\n",
    "It is possible to derive your own counting function to work with this toolkit. The class should implement a `run(self, sen, word, rows, columns)` function, which takes as arguments the sentence containing the cooccurrence (for determining context via dependency relations), the word being counted, and the lists of rows and columns. Any additional parameters hould be set during `__init__()`."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.14"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
