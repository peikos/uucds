# UUCDS
This repository contains my code used for calculating verb-matrices for Dutch transitive verbs from the LASSY-Small corpus (Universal Dependencies version). Documentation and code examples are provided in the enclosed Jupyter Notebook. In order to use this library, the following dependencies are required:

- Python 2.7
- [NumPy](http://www.numpy.org)
- [SciPy](https://www.scipy.org)
- [CONLLU](https://github.com/EmilStenstrom/conllu)
- [DISSECT](http://clic.cimec.unitn.it/composes/toolkit)

Further documentation can be found in the Jupyter Notebook.

## Alternative: using Docker
Alternatively, the provided `Dockerfile` can be used to generate a container which will install dependencies, copy the source files and run the Jupyter Notebook on port 8888. To use this approach, execute the following commands once within the root directory of this repository.

```
$ docker build -t uucds .
$ docker run -it --name uucds -p 8888:8888 uucds
```

To restart the image, simply use `docker start -it uucds`. The container, when running, can be accessed using `docker exec -it uucds /bin/bash` (please refer to the relevant [Docker documentation](https://docs.docker.com/engine/reference/commandline/exec) for more information on using Docker).

**Note: ** the approach detailed here is persistent only when `start`ing (and `stop`ping) the same Docker container; removing the image and rerunning (using `run`) will not persist data. Persistence can be achieved using [Docker volumes](https://docs.docker.com/storage/volumes).
